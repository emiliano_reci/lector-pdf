import React, { useState } from 'react';
import { Document, pdfjs, Page } from 'react-pdf';
import uml from '../uml.pdf'

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

//const miPdfDocument = 'https://arxiv.org/pdf/quant-ph/0410100.pdf';
//const url_pdf ="http://www.africau.edu/images/default/sample.pdf";

 const  EjemploUno2=({mySize})=> {
  let [numPages, setNumPage] = useState(null);
  let [pageNumber, setPageNumber] = useState(1);
  let [zoom, setZoom] = useState(1);
  //let sizeFinal = window.innerWidth;
 
  const onDocumentLoadSuccess = (document) => {
    const { numPages } = document;
    setNumPage(numPages);
    setPageNumber(pageNumber);
  }

  const ChangePageUp = () => {
    setPageNumber(pageNumber +1)
  }
  const ChangePageDown = () => {
    setPageNumber(pageNumber - 1)
  }
  
  const ZoomUp = () => {
    setZoom(zoom + 0.25)
  }
  const ZoomDown = () => {
    setZoom(zoom - 0.25)
  } 

//Solucion al problema de cors, usando un proxy
const url_proxy = 'https://cors-anywhere.herokuapp.com/http://www.africau.edu/images/default/sample.pdf'

   return (
     <>
       <Document
         file={url_proxy}
         onLoadSuccess={onDocumentLoadSuccess}
       >
         <Page
           pageNumber={pageNumber}
           width={mySize}
         />
       </Document>
       <p>Page {pageNumber} of {numPages}</p>

       <button onClick={ChangePageUp}>
         Pagina Siguiente
        </button>
       <button onClick={ChangePageDown}>
         Pagina Anterior
        </button>
       <br />
       <button onClick={ZoomUp}>
         zoom +
        </button>
       <button onClick={ZoomDown}>
         zoom -
        </button>
       <a href={uml} download>Click to download</a>
     </>
   );
}

export default EjemploUno2;