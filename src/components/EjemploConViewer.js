import React from 'react'
import PDFViewer from 'pdf-viewer-reactjs'
import uml from '../uml.pdf'
import './styles.css';

const miPdfDocument = 'https://arxiv.org/pdf/quant-ph/0410100.pdf';
const url_nueva ="http://www.africau.edu/images/default/sample.pdf"
const ExamplePDFViewer = () => {
 
    return (
        <>
            <PDFViewer
                document={{
                    url: url_nueva,

                }}
                scale={1}
                page={1}
                canvasCss="pdf"
                css="pdf-container"
            />
            <button className="btn-download">
                <a
                 className="a-btn-download"
                 href={uml}
                 download
                 >
                    Click to download
            </a>
            </button>
        </>
    )
}

export default ExamplePDFViewer